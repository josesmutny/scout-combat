extends RigidBody2D

export var is_on_right := false
const floor_y = 500

export var jump = 200
export var move_x = 50
#var move = Vector2()
#export var gravity = 10
#export var max_run_speed = 10
#
#export var acc = 1
#export var dec = 0.5

func _physics_process(delta):
	if $Nohy.global_position.y > floor_y:
		global_position.y -= $Nohy.global_position.y - floor_y
	
	if $Nohy.global_position.y == floor_y :
		if Input.is_key_pressed(KEY_W if is_on_right else KEY_UP):
			apply_central_impulse(Vector2(0, -jump))
	
	if Input.is_key_pressed(KEY_D if is_on_right else KEY_RIGHT):
		apply_central_impulse(Vector2(move_x, 0))
	if Input.is_key_pressed(KEY_A if is_on_right else KEY_LEFT):
		apply_central_impulse(Vector2(-move_x, 0))
#	else:
#		move.x = max(abs(move.x) - dec, 0) * (1 if move.x > 0 else -1)
	
#	move.y += gravity
#	move_and_slide(move, Vector2.UP)
